'use strict';

const getUsers = async () => {
    return await fetch('http://jsonplaceholder.typicode.com/users')
        .then(res => {
            try {
                return res.json();
            } catch (error) {
                console.error('Ошибка получения пользователей:', error);
                return [];
            }
        })
        .then(res => res);
};

const getPosts = async () => {
    return await fetch('http://jsonplaceholder.typicode.com/posts')
        .then(res =>  {
            try {
                return res.json();
            } catch (error) {
                console.error('Ошибка получения постов:', error);
                return [];
            }
        })
        .then(res => res);
};

const getComments = async (postId) => {
    return await fetch(`http://jsonplaceholder.typicode.com/posts/${ postId }/comments`)
        .then(res =>  {
            try {
                return res.json();
            } catch (error) {
                console.error('Ошибка получения комментариев:', error);
                return [];
            }
        })
        .then(res => res);
};

const getObj = async () => {
    const users = await getUsers();
    const posts = await getPosts();

    const usersNew = Promise.all(users.map(async user => {
        const address = `${ user.address.city }, ${ user.address.street }, ${ user.address.suite }`;
        const userPosts = await Promise.all(posts.filter(post => post.userId === user.id).map(async post => {
            const title_crop = post.title.slice(0, 20) + '...';

            const newPost = {
                id: post.id,
                title: post.title,
                title_crop,
                body: post.body
            }
            if (user.name === "Ervin Howell") {
                const comments = await getComments(post.id);

                newPost.comments = comments.filter(
                    comment => (comment.postId === post.id)
                );
            }
            return newPost;
        })).then(res => res);

        return {
            id: user.id,
            name: user.name,
            email: user.email,
            address,
            website: 'https://' + user.website,
            company: user.company.name,
            posts: userPosts
        }
    })) 
    return usersNew
};
getObj().then(users => console.log('users', users)).catch(error => console.error('Ошибка:', error));



