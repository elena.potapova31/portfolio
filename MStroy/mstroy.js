// Есть массив объектов, которые имеют поля id и parent, 
//через которые их можно связать в дерево и некоторые произвольные поля. 
//id может быть как числом, так и строкой. Порядок id не гарантируется, 
//изначально отсутствует какой либо принцип сортировки. 
//Поле type не влияет ни на что, просто отображает возможность наличия какой-то полезной нагрузки в айтемах.

// НУЖНО написать класс, который принимает в конструктор массив этих объектов и реализует следующие методы:
//  *  - getAll() Должен возвращать изначальный массив элементов.
//  *  - getItem(id) Принимает id элемента и возвращает сам объект элемента;
//  *  - getChildren(id) Принимает id элемента и возвращает массив элементов, являющихся дочерними для того элемента,
//  * чей id получен в аргументе. Если у элемента нет дочерних, то должен возвращаться пустой массив;
//  *  - getAllChildren(id) Принимает id элемента и возвращает массив элементов, являющихся прямыми дочерними элементами того,
//  * чей id получен в аргументе + если у них в свою очередь есть еще дочерние элементы, они все тоже будут включены в результат,
//  * и так до самого глубокого уровня.
//  *  - getAllParents(id) Принимает id элемента и возвращает массив из цепочки родительских элементов,
//  * начиная от самого элемента, чей id был передан в аргументе и до корневого элемента,
//  * т.е. должен получиться путь элемента наверх дерева через цепочку родителей к корню дерева. 
// * в результате getAllParents ПОРЯДОК ЭЛЕМЕНТОВ ВАЖЕН!
 
// ТРЕБОВАНИЕ: максимальное быстродействие, следовательно, минимальное количество обходов массива при операциях,
//  * в идеале, прямой доступ к элементам без поиска их в массиве.

//  ПЛЮСАМИ будет:
//  *  - написание этого класса на TypeScript
//  *  - написание тестов для функционала методов


class TreeStore {
	constructor(items) {
		this.items = items;
		this.itemsObj = {};
		this.children = new Map();

		this.items.forEach((item) => {
			this.itemsObj[item.id] = item;

			if (this.children.has(item.parent)) {
				this.children.get(item.parent).push(item);
			} else {
				this.children.set(item.parent, [item]);
			}
		});
	}

	getAll() {
		return this.items;
	}

	getItem(id) {
		return this.itemsObj[id];
	}

	getChildren(id) {
		return this.children.get(id) || [];
	}

	getAllChildren(id) {
		let children = this.getChildren(id);

		for (const child of children) {
			if (this.getChildren(child.id).length > 0) {
				children = children.concat(this.getAllChildren(children.id));
			}
		}

		return children;
	}

	getAllParents(itemId) {
		const parObj = {};
		this.getAllParentsRecurs(itemId, parObj);
		return Object.values(parObj).reverse();
	}

	getAllParentsRecurs(itemId, parents) {
		const child = this.getItem(itemId);
		if (child) {
			parents[child.id] = child;
			if (child.parent) {
				this.getAllParentsRecurs(child.parent, parents);
			}
		} else {
			return parents;
		}
	}
}

const items = [
  { id: 1, parent: 'root' },
  { id: 2, parent: 1, type: 'test' },
  { id: 3, parent: 1, type: 'test' },
  { id: 4, parent: 2, type: 'test' },
  { id: 5, parent: 2, type: 'test' },
  { id: 6, parent: 2, type: 'test' },
  { id: 7, parent: 4, type: null },
  { id: 8, parent: 4, type: null },
];

const ts = new TreeStore(items);